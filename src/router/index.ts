import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView
    },
    {
      path: '/chat',
      name: 'chat',
      
      component: () => import('../views/ChatView.vue')
    },
    {
      path: '/address-book',
      name: 'address-book',
      
      component: () => import('../views/AndressBookView.vue')
    },
    {
      path: '/home3',
      name: 'home3',
      
      component: () => import('../views/Home3/HomeView.vue')
    },

  ]
})

export default router
